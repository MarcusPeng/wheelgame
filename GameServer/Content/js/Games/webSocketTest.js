﻿let webSocket, gameRoom;

function onStartButtonClick() {
    let playerName = document.getElementById('playerName').value;
    const wsUrl = `ws://${window.location.host}/Games/WheelGameSocket?playerName=${playerName}&gender=0`;
    webSocket = new WebSocket(wsUrl);
    webSocket.onopen = () => { console.log('open'); };
    webSocket.onclose = () => { console.log('close'); };
    webSocket.onmessage = onmessage;
    webSocket.onerror = () => { console.log('error'); };
}

function onmessage(message) {
    console.log(message);
    let logLi = document.createElement('li');
    logLi.textContent = message.data;
    document.getElementById('logList').appendChild(logLi);

    let data = JSON.parse(message.data);
    if (data.EventType === 'OnOpen') {
        gameRoom = data.Data.GameRoom;
    }
}

function onEndButtonClick() {
    webSocket.close();
}

function onHistoryButtonClick() {
    console.log('History');
    let message = {
        EventType: 'OnHistory',
        Data: {
            GameRoom: gameRoom
        }
    };
    console.log(message);
    webSocket.send(JSON.stringify(message));
}

function onSendButtonClick() {
    console.log('send');
    let cardColor = document.getElementById('cardColor').value;
    let message = {
        EventType: 'OnCardBuy',
        Data: {
            CardColor: cardColor
        }
    };
    webSocket.send(JSON.stringify(message));
}


document.getElementById('startButton').addEventListener('click', onStartButtonClick);
document.getElementById('endButton').addEventListener('click', onEndButtonClick);
document.getElementById('sendButton').addEventListener('click', onSendButtonClick);
document.getElementById('historyButton').addEventListener('click', onHistoryButtonClick);

