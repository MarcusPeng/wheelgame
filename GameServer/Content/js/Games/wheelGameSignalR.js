﻿

Vue.component('Player', {
    props: {
        player: {
            type: Object,
            required: true
        }
    },
    template: `
        <div v-if="player">
            <br />
            <label> Name: {{player.Name}}</label>,
            <label> Coins: {{player.Coins}}</label>,
            <label> Transportation: {{player.Transportation == '0' ? 'SignalR' : 'WebSocket'}}</label>
            <br />
            <label v-for="(value, key) in player.OwnCards"> {{key}}: {{value}} </label>
            <br />
        </div>`
});


Vue.component('GameAction', {
    props: {
        wheelGameHub: Object,
        cardList: Array
    },
    created: function () {
        this.wheelGameHub.client.onCardBuy = this.onCardBuy;
    },
    data: function () {
        return {
            buyCardValue: 0
        }
    },
    methods: {
        endGame: function () {
            this.wheelGameHub.server.onClose();
            $.connection.hub.stop();
            // TODO: Show Player Name Input
        },
        buyCard: function () {
            this.wheelGameHub.server.onCardBuy(this.buyCardValue);
        },
        onCardBuy: function (data) {
            this.$emit('changePlayerState', data);
        }
    },
    template: `
        <div>
            <select v-model="buyCardValue">
                <option v-for="(item, index) in cardList" :value="item.value">{{item.name}}</option>
            </select>
            <input type="button" value="Buy" v-on:click="buyCard" />
            <input type="button" value="Exit" v-on:click="endGame" />
        </div>`
});


Vue.component('GameSection', {
    props: {
        playerName: String,
    },
    created: function () {
        this.wheelGameHub.client.onOpen = this.onOpen;
        this.wheelGameHub.client.onStart = this.onStart;
        this.wheelGameHub.client.onTickStart = this.onTickStart;
        this.wheelGameHub.client.onBuyStart = this.onBuyStart;
        this.wheelGameHub.client.onBuyEnd = this.onBuyEnd;
        this.wheelGameHub.client.onRotateCountdownStart = this.onRotateCountdownStart;
        this.wheelGameHub.client.onRotateStart = this.onRotateStart;
        this.wheelGameHub.client.onShowResultStart = this.onShowResultStart;
    },
    data: function () {
        return {
            wheelGameHub: $.connection.wheelGameHub,
            cardList: [
                { name: 'Blue', value: 0, color: '#07b' },
                { name: 'Green', value: 1, color: '#2a2' },
                { name: 'Red', value: 2, color: '#2a2' },
                { name: 'Yellow', value: 3, color: '#ff0' },
                { name: 'Purple', value: 4, color: '#f0a' },
                { name: 'Gold', value: 5, color: 'gold' },
                { name: 'Diamond', value: 6, color: '#B9F2FF' }
            ],
            openData: null,
            tickData: null,
            rotateData: null,
            stepState: '',
            resultColor: null
        }
    },
    methods: {
        onOpen: function (data) {
            console.log(data);
            this.openData = data;
        },
        onStart: function () {
            console.log('Start');
            this.resultColor = null;
        },
        onTickStart: function (data) {
            //console.log(data);
            this.tickData = data;
        },
        onBuyStart: function () {
            console.log('BuyStart');
            this.stepState = 'Buy';
        },
        onBuyEnd: function () {
            console.log('BuyEnd');
        },
        onRotateCountdownStart: function (data) {
            console.log(data);
            this.rotateData = data;
            this.stepState = 'Rotate Countdown';
        },
        onRotateStart: function () {
            console.log('RotateStart');
            this.stepState = 'Rotate Start';
        },
        onShowResultStart: function (data) {
            console.log(data);
            this.stepState = 'Show Result';
            if (this.rotateData) {
                let resultColorValue = this.openData.Wheel[this.rotateData.ResultIndex];
                this.resultColor = this.cardList.find(function (element) {
                    return element.value === resultColorValue;
                });
            }
        },
        changePlayerState: function (player) {
            let targetPlayer = this.tickData.Players.find(function (element) { return element.PlayerId === player.PlayerId; });
            targetPlayer.Coins = player.Coins;
            targetPlayer.OwnCards = player.OwnCards;
        }
    },
    mounted: function () {
        let $wheelGameHub = this.wheelGameHub;
        $.connection.hub.disconnected(function () {
            console.log('disconnected');
        });
        $.connection.hub.qs = { playerName: this.playerName };
        $.connection.hub.start().done(function () {
            window.onbeforeunload = function () {
                $wheelGameHub.server.onClose();
                $.connection.hub.stop();
            }
        });
    },
    template: `
        <div>
            Hello {{playerName}}!
            <GameAction v-bind:wheelGameHub="wheelGameHub" v-bind:cardList="cardList" @changePlayerState="changePlayerState" />
            <div v-if="tickData">
                <label>Game Left Seconds: {{tickData.LeftSeconds}}</label><br />
                <label>Step Left Seconds: {{tickData.StepLeftSeconds}}</label><br />
                <label>Step State: {{stepState}}</label><br />
                <label v-if="resultColor" v-bind:style="{color: resultColor.color}">Result: {{resultColor.name}}</label>
            </div>
            <div v-if="tickData">
                <Player v-for="(item, index) in tickData.Players" :key="item.PlayerId" v-bind:player="item" />
            </div>
        </div>`
});


Vue.component('App', {
    data: function () {
        return {
            isNameValid: false,
            playerName: '',
        }
    },
    methods: {
        startGame: function () {
            this.isNameValid = this.playerName.length > 0;
        }
    },
    template: `
        <div>
            <div v-if="!isNameValid">
                <label>Player Name: <input type="text" v-model="playerName" placeholder="Plaese input your name" /></label>
                <input type="button" value="Start" v-on:click="startGame" />
            </div>
            <GameSection v-if="isNameValid" v-bind:playerName="playerName" />
        </div>`
});


new Vue({
    el: '#app',
    template: '<App />'
});

