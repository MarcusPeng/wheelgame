using System.Web.Mvc;
using Unity;
using Unity.Mvc5;
using GameServer.Services;
using GameServer.Models.DbContexts;
using GameServer.Models.Entities;
using GameServer.Models.Repositories;
using GameServer.Models.UnitOfWorks;

namespace GameServer
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType<IGameServerContext, GameServerContext>();
            container.RegisterType<IRepository<PlayerEntity, string>, EFGameServerRepository<PlayerEntity, string>>();
            container.RegisterType<IRepository<GameEntity, string>, EFGameServerRepository<GameEntity, string>>();
            container.RegisterType<IRepository<GameResultEntity, int>, EFGameServerRepository<GameResultEntity, int>>();
            container.RegisterType<IGameServerUnitOfWork, GameServerUnitOfWork>();
            container.RegisterType<IWheelGameService, WheelGameService>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            MvcApplication.Container = container;
        }
    }
}