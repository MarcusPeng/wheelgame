﻿using System.Web;
using System.Web.Optimization;

namespace GameServer
{
    public class BundleConfig
    {
        // 如需統合的詳細資訊，請瀏覽 https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Content/js/lib/jquery-1.6.4.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/signalR").Include(
                        "~/Content/js/lib/jquery.signalR-2.2.3.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/wheelGameSignalR").Include(
                        "~/Content/js/Games/wheelGameSignalR.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                      "~/Content/css/site.css"));

            BundleTable.EnableOptimizations = true;
        }
    }
}
