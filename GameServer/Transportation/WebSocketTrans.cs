﻿using GameServer.Games;
using GameServer.Models;
using Microsoft.Web.WebSockets;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.WebSockets;

namespace GameServer.Transportation
{
    

    public class WebSocketTrans : WebSocketHandler
    {
        public delegate void OnCardBuyEventHandler(object sender, OnCardBuyEventArgs e);
        public delegate void OnOpenventHandler(object sender, OnOpenEventArgs e);
        public delegate void OnHistoryHandler(object sender, OnHistoryEventArgs e);
        public event OnOpenventHandler OnOpenEvent;
        public event EventHandler OnCloseEvent;
        public event OnCardBuyEventHandler OnCardBuyEvent;
        public event OnHistoryHandler OnHistoryEvent;

        public string PlayerId { get; set; }

        public WebSocketTrans()
        {

        }
        public override void OnOpen()
        {
            base.OnOpen();
            PlayerId = WebSocketContext.SecWebSocketKey;
            NameValueCollection QueryStrings = base.WebSocketContext.QueryString;
            PlayerInfo.GenderType Gender = PlayerInfo.GenderType.Unknown;
            if (QueryStrings["gender"] != null && int.TryParse(QueryStrings["gender"], out int gender))
            {
                Gender = gender < 3 ? (PlayerInfo.GenderType)gender : PlayerInfo.GenderType.Unknown;
            }
            OnOpenEventArgs e = new OnOpenEventArgs
            {
                PlayerId = PlayerId,
                Name = QueryStrings["playerName"],
                Gender = Gender
            };

            OnOpenEvent?.Invoke(this, e);
        }
        public override void OnClose()
        {
            OnCloseEvent?.Invoke(this, EventArgs.Empty);
            base.OnClose();
        }
        public override void OnMessage(string message)
        {
            Message ReceiveMessage = new Message();
            try
            {
                ReceiveMessage = Newtonsoft.Json.JsonConvert.DeserializeObject<Message>(message);
            }
            catch (Exception) { }

            switch (ReceiveMessage.EventType)
            {
                case "OnCardBuy":
                    if (ReceiveMessage.Data.CardColor != null)
                    {
                        if (int.TryParse(ReceiveMessage.Data.CardColor.ToString(), out int CardColor) && Enum.IsDefined(typeof(WheelGame.WheelColor), CardColor))
                        {
                            OnCardBuyEvent?.Invoke(this, new OnCardBuyEventArgs { CardColor = (WheelGame.WheelColor)CardColor });
                        }
                    }
                    break;

                case "OnHistory":
                    if (!string.IsNullOrEmpty(ReceiveMessage.Data.GameRoom.ToString()))
                    {
                        OnHistoryEvent?.Invoke(this, new OnHistoryEventArgs { GameRoom = ReceiveMessage.Data.GameRoom });
                    }
                    break;

                default:
                    break;
            }
        }
    }

    
    public class OnOpenEventArgs : EventArgs
    {
        public string PlayerId { get; set; }
        public string Name { get; set; }
        public PlayerInfo.GenderType Gender { get; set; }
    }
    public class OnCardBuyEventArgs : EventArgs
    {
        public WheelGame.WheelColor CardColor { get; set; }
    }
    public class OnHistoryEventArgs : EventArgs
    {
        public string GameRoom;
    }
}