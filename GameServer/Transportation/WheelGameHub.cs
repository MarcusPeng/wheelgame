﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using GameServer.Games;
using GameServer.Models;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Owin;

[assembly: OwinStartup(typeof(GameServer.Startup))]
namespace GameServer.Transportation
{
    public class WheelGameHub : Hub
    {
        private readonly Services.IWheelGameService _WheelGameService;

        public WheelGameHub(Services.IWheelGameService wheelGameService)
        {
            _WheelGameService = wheelGameService;
        }

        public override Task OnConnected()
        {
            Microsoft.AspNet.SignalR.Hosting.INameValueCollection QueryStrings = Context.QueryString;
            PlayerInfo.GenderType Gender = PlayerInfo.GenderType.Unknown;
            if (QueryStrings["gender"] != null && int.TryParse(QueryStrings["gender"], out int gender))
            {
                Gender = gender < 3 ? (PlayerInfo.GenderType)gender : PlayerInfo.GenderType.Unknown;
            }

            _WheelGameService.OnOpen(QueryStrings["playerName"], Context.ConnectionId, Gender, PlayerInfo.TransportationType.SignalR);

            return base.OnConnected();
        }

        public override Task OnReconnected()
        {
            return base.OnReconnected();
        }
    
        public void OnClose()
        {
            _WheelGameService.OnClose(Context.ConnectionId, PlayerInfo.TransportationType.SignalR);
        }

        public void OnCardBuy(int CardColor)
        {
            if (Enum.IsDefined(typeof(WheelGame.WheelColor), CardColor))
            {
                _WheelGameService.OnCardBuy(Context.ConnectionId, (WheelGame.WheelColor)CardColor, PlayerInfo.TransportationType.SignalR);
            }
        }
    }
}