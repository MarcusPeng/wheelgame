﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GameServer.Models;

namespace GameServer.Games
{
    public class WheelGame
    {
        #region Enum
        public enum WheelColor { Blue, Green, Red, Yellow, Purple, Gold, Diamond }
        public enum WheelEventType
        {
            OnCreate, OnStart, OnTickStart, OnTickEnd, OnBuyStart, OnBuyEnd, OnRotateCountdownStart,
            OnRotateCountdownEnd, OnRotateStart, OnRotateEnd, OnShowResultStart, OnShowResultEnd, OnEnd, OnExit
        }
        #endregion

        #region Event
        public event EventHandler OnCreate;
        public event EventHandler OnStart;
        public event EventHandler OnTickStart;
        public event EventHandler OnTickEnd;
        public event EventHandler OnBuyStart;
        public event EventHandler OnBuyEnd;
        public event EventHandler OnRotateCountdownStart;
        public event EventHandler OnRotateCountdownEnd;
        public event EventHandler OnRotateStart;
        public event EventHandler OnRotateEnd;
        public event EventHandler OnShowResultStart;
        public event EventHandler OnShowResultEnd;
        public event EventHandler OnEnd;
        public event EventHandler OnExit;
        #endregion

        #region Prop
        /// <summary>
        /// 遊戲房編號
        /// </summary>
        public string GameRoom { get; set; }
        /// <summary>
        /// 遊戲場次編號
        /// </summary>
        public int GameSeq { get; private set; }
        /// <summary>
        /// 轉盤結果Index
        /// </summary>
        public int ResultIndex { get; private set; }
        /// <summary>
        /// 總剩餘秒數
        /// </summary>
        public int LeftSeconds { get; private set; }
        /// <summary>
        /// 階段剩餘秒數
        /// </summary>
        public int StepLeftSeconds { get; private set; }
        /// <summary>
        /// 遊戲總秒數
        /// </summary>
        public int TotalSeconds => GameSetting.BuyingSeconds + GameSetting.RotateCountdownSeconds + GameSetting.RotateSeconds + GameSetting.ShowResultSeconds;
        /// <summary>
        /// 現階段是否可購買
        /// </summary>
        public bool CanBuy => LeftSeconds > TotalSeconds - GameSetting.BuyingSeconds;
        /// <summary>
        /// 遊戲設定
        /// </summary>
        public WheelGameSetting GameSetting { get; private set; } 
        #endregion

        private CancellationTokenSource _CancellationTokenSource;

        public WheelGame(WheelGameSetting wheelGameSetting = null)
        {
            _CancellationTokenSource = new CancellationTokenSource();
            GameSetting = wheelGameSetting ?? new WheelGameSetting();
        }

        /// <summary>
        /// 開始遊戲
        /// </summary>
        public void Start()
        {
            LeftSeconds = TotalSeconds;
            CancellationToken cancellationToken = _CancellationTokenSource.Token;
            Task<int> RandomColorTask = Task.Run(() => 0);

            Task.Run(async() =>
            {
                OnCreate?.Invoke(this, EventArgs.Empty);

                while (!cancellationToken.IsCancellationRequested)
                {
                    OnTickStart?.Invoke(this, EventArgs.Empty);

                    //Buy
                    if (LeftSeconds.Equals(TotalSeconds))
                    {
                        RandomColorTask = GetRandomColorIndexAsync();
                        StepLeftSeconds = GameSetting.BuyingSeconds;
                        OnStart?.Invoke(this, EventArgs.Empty);
                        OnBuyStart?.Invoke(this, EventArgs.Empty);
                    }
                    //Rotate Countdown
                    else if (LeftSeconds.Equals(TotalSeconds - GameSetting.BuyingSeconds))
                    {
                        ResultIndex = await RandomColorTask;
                        StepLeftSeconds = GameSetting.RotateCountdownSeconds;
                        OnBuyEnd?.Invoke(this, EventArgs.Empty);
                        OnRotateCountdownStart?.Invoke(this, EventArgs.Empty);
                    }
                    //Rotate Start
                    else if (LeftSeconds.Equals(TotalSeconds - GameSetting.BuyingSeconds - GameSetting.RotateCountdownSeconds))
                    {
                        StepLeftSeconds = GameSetting.RotateSeconds;
                        OnRotateCountdownEnd?.Invoke(this, EventArgs.Empty);
                        OnRotateStart?.Invoke(this, EventArgs.Empty);
                    }
                    //ShowResult
                    else if (LeftSeconds.Equals(TotalSeconds - GameSetting.BuyingSeconds - GameSetting.RotateCountdownSeconds - GameSetting.RotateSeconds))
                    {
                        StepLeftSeconds = GameSetting.ShowResultSeconds;
                        OnRotateEnd?.Invoke(this, EventArgs.Empty);
                        OnShowResultStart?.Invoke(this, EventArgs.Empty);
                    }
                    //End
                    else if (LeftSeconds.Equals(1))
                    {
                        OnShowResultEnd?.Invoke(this, EventArgs.Empty);
                        OnEnd?.Invoke(this, EventArgs.Empty);
                        GameSeq++;
                    }

                    Thread.Sleep(1000);
                    LeftSeconds = LeftSeconds.Equals(1) ? TotalSeconds : --LeftSeconds;
                    StepLeftSeconds -= 1;
                    OnTickEnd?.Invoke(this, EventArgs.Empty);
                }

                OnExit?.Invoke(this, EventArgs.Empty);
                cancellationToken.ThrowIfCancellationRequested();

            }, cancellationToken);
        }

        /// <summary>
        /// 遊戲結束
        /// </summary>
        public void Exit()
        {
            _CancellationTokenSource.Cancel();
        }

        /// <summary>
        /// 產生結果
        /// </summary>
        /// <returns></returns>
        private async Task<int> GetRandomColorIndexAsync()
        {
            //return await Task.Run(() => {
            //    return (DateTime.Now.Millisecond % GameSetting.Wheel.Length);
            //});

            string RandomSeedUrl = "http://keepcalm246.hopto.org:8003/api/RandomSeed";
            string RandomSeed = string.Empty;
            int RandomSeedInt = 0;

            using (WebClient webClient = new WebClient())
            {
                RandomSeed = await webClient.DownloadStringTaskAsync(RandomSeedUrl);
            }
            int.TryParse(RandomSeed, out RandomSeedInt);

            //Random seed api will return 1~54, but we neet wheel index.
            return (RandomSeedInt - 1) % GameSetting.Wheel.Length;
        }

    }
}
