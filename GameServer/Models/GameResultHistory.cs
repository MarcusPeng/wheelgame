﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameServer.Models
{
    public class GameResultHistory
    {
        public string GameRoom { get; set; }
        public int ResultColor { get; set; }
        public int GameSeq { get; set; }
        public List<PlayerInfo> Players { get; set; }
    }
}