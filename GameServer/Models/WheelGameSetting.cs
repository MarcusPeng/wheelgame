﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static GameServer.Games.WheelGame;

namespace GameServer.Models
{
    public class WheelGameSetting
    {
        public WheelGameSetting()
        {
            CardWinningSetting = new CardWinning();
        }

        private int _BuyingSeconds;
        /// <summary>
        /// 購買秒數
        /// </summary>
        public int BuyingSeconds
        {
            get => _BuyingSeconds <= 0 ? _DefaultBuyingSeconds : _BuyingSeconds;
            set => _BuyingSeconds = value;
        }

        private int _RotateCountdownSeconds;
        /// <summary>
        /// 轉盤倒數開始轉動秒數
        /// </summary>
        public int RotateCountdownSeconds
        {
            get => _RotateCountdownSeconds <= 0 ? _DefaultRotateCountdownSeconds : _RotateCountdownSeconds;
            set => _RotateCountdownSeconds = value;
        }

        private int _RotateSeconds;
        /// <summary>
        /// 轉盤轉動秒數
        /// </summary>
        public int RotateSeconds
        {
            get => _RotateSeconds <= 0 ? _DefaultRotateSeconds : _RotateSeconds;
            set => _RotateSeconds = value;
        }

        private int _ShowResultSeconds;
        /// <summary>
        /// 顯示結果秒數
        /// </summary>
        public int ShowResultSeconds
        {
            get => _ShowResultSeconds <= 0 ? _DefaultShowResultSeconds : _ShowResultSeconds;
            set => _ShowResultSeconds = value;
        }

        private WheelColor[] _Wheel;
        /// <summary>
        /// 轉盤顏色設定
        /// </summary>
        public WheelColor[] Wheel
        {
            get => _Wheel ?? _DefaultWheel;
            set => _Wheel = value;
        }

        private CardWinning _CardWinning;
        /// <summary>
        /// 賠率設定
        /// </summary>
        public CardWinning CardWinningSetting
        {
            get => _CardWinning ?? new CardWinning();
            set => _CardWinning = value;
        }


        private const int _DefaultBuyingSeconds = 35;
        private const int _DefaultRotateCountdownSeconds = 5;
        private const int _DefaultRotateSeconds = 10;
        private const int _DefaultShowResultSeconds = 10;
        private readonly WheelColor[] _DefaultWheel = new WheelColor[]
        {
            WheelColor.Diamond, WheelColor.Blue, WheelColor.Green, WheelColor.Blue, WheelColor.Red, WheelColor.Green, WheelColor.Blue, WheelColor.Yellow, WheelColor.Blue, WheelColor.Red,
            WheelColor.Blue, WheelColor.Green, WheelColor.Blue, WheelColor.Purple, WheelColor.Blue, WheelColor.Green, WheelColor.Blue, WheelColor.Red, WheelColor.Blue, WheelColor.Green,
            WheelColor.Blue, WheelColor.Yellow, WheelColor.Blue, WheelColor.Red, WheelColor.Blue, WheelColor.Green, WheelColor.Blue, WheelColor.Gold, WheelColor.Green, WheelColor.Blue, WheelColor.Green,
            WheelColor.Blue, WheelColor.Green, WheelColor.Blue, WheelColor.Yellow, WheelColor.Blue, WheelColor.Red, WheelColor.Blue, WheelColor.Green, WheelColor.Blue, WheelColor.Purple, WheelColor.Blue,
            WheelColor.Green, WheelColor.Blue, WheelColor.Red, WheelColor.Blue, WheelColor.Yellow, WheelColor.Blue, WheelColor.Green, WheelColor.Blue, WheelColor.Red, WheelColor.Blue, WheelColor.Green, WheelColor.Blue
        };

        public class CardWinning
        {
            private int _Blue;
            public int Blue { get { return _Blue <= 0 ? _DefaultBlue : _Blue; } set { _Blue = value; } }

            private int _Green;
            public int Green { get { return _Green <= 0 ? _DefaultGreen : _Green; } set { _Green = value; } }

            private int _Red;
            public int Red { get { return _Red <= 0 ? _DefaultRed : _Red; } set { _Red = value; } }

            private int _Yellow;
            public int Yellow { get { return _Yellow <= 0 ? _DefaultYellow : _Yellow; } set { _Yellow = value; } }

            private int _Purple;
            public int Purple { get { return _Purple <= 0 ? _DefaultPurple : _Purple; } set { _Purple = value; } }

            private int _Gold;
            public int Gold { get { return _Gold <= 0 ? _DefaultGold : _Gold; } set { _Gold = value; } }

            private int _Diamond;
            public int Diamond { get { return _Diamond <= 0 ? _DefaultDiamond : _Diamond; } set { _Diamond = value; } }

            private const int _DefaultBlue = 2;
            private const int _DefaultGreen = 4;
            private const int _DefaultRed = 7;
            private const int _DefaultYellow = 13;
            private const int _DefaultPurple = 26;
            private const int _DefaultGold = 51;
            private const int _DefaultDiamond = 51;
        }
    }
}