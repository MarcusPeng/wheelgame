﻿using Microsoft.Web.WebSockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GameServer.Games;

namespace GameServer.Models
{
    public class WheelGameInfo
    {
        public List<PlayerInfo> Players { get; set; }
        public WheelGame Game { get; set; }
        public int MaxPlayer => 7;
    }
}