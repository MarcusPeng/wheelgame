﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameServer.Models
{
    public class PlayerInfo
    {
        public enum GenderType { Unknown, Male, Female }

        public PlayerInfo()
        {
            OwnCards = new CardCount();
            Gender = GenderType.Unknown;
        }

        public string PlayerId { get; set; }
        public string Name { get; set; }
        public GenderType Gender { get; set; }
        public int Coins { get; set; }
        public CardCount OwnCards { get; set; }
        public TransportationType Transportation { get; set; }
        public enum TransportationType { SignalR, WebSocket }

        public class CardCount
        {
            public int Blue { get; set; }
            public int Green { get; set; }
            public int Red { get; set; }
            public int Yellow { get; set; }
            public int Purple { get; set; }
            public int Gold { get; set; }
            public int Diamond { get; set; }
            public void Clear()
            {
                Blue = 0;
                Green = 0;
                Red = 0;
                Yellow = 0;
                Purple = 0;
                Gold = 0;
                Diamond = 0;
            }
        }
    }
}