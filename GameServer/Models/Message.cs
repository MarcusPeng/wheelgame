﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameServer.Models
{
    public class Message
    {
        public string EventType { get; set; }
        public dynamic Data { get; set; }
    }
}