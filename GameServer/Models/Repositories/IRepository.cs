﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Models.Repositories
{
    public interface IRepository<TEntity, TKey> where TEntity : class
    {
        TEntity Find(TKey Key);
        IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> Predicate);
        void Insert(TEntity Entity);
        void Delete(TKey Key);
        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}
