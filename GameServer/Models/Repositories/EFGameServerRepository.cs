﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using GameServer.Models.DbContexts;

namespace GameServer.Models.Repositories
{
    public class EFGameServerRepository<TEntity, Key> : IRepository<TEntity, Key> where TEntity : class
    {
        private IGameServerContext _Context { get; set; }

        public EFGameServerRepository(IGameServerContext dbContext)
        {
            _Context = dbContext;
        }
        public void Delete(Key Key)
        {
            TEntity entity = Find(Key);
            if (entity != null)
            {
                _Context.Set<TEntity>().Remove(entity);
            }
        }

        public TEntity Find(Key Key)
        {
            TEntity Result = null;
            try
            {
                Result = _Context.Set<TEntity>().Find(Key);
            }
            catch (Exception)
            {

            }
            return Result;
        }

        public void Insert(TEntity Entity)
        {
            _Context.Set<TEntity>().Add(Entity);
        }

        public IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> Predicate)
        {
            return _Context.Set<TEntity>().Where(Predicate).AsQueryable();
        }

        public int SaveChanges()
        {
            return _Context.SaveChanges();
        }
        public async Task<int> SaveChangesAsync()
        {
            return await _Context.SaveChangesAsync();
        }
    }
}