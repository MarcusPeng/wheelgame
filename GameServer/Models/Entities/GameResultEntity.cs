﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GameServer.Models.Entities
{
    [Table("GameResult")]
    public class GameResultEntity
    {
        [Key]
        public int GameResultId { get; set; }
        [Required, MaxLength(50)]
        public string GameRoom { get; set; }
        [Required]
        public int GameSeq { get; set; }
        [Required]
        public int ResultColor { get; set; }
        [Required, MaxLength(50)]
        public string PlayerId { get; set; }
        [Required]
        public int PlayerCoins { get; set; }
        [Required, MaxLength(4000)]
        public string PlayerOwnCards { get; set; }
        [Required]
        public DateTime CreateTime { get; set; }


        [ForeignKey("GameRoom")]
        public virtual GameEntity Game { get; set; }
        [ForeignKey("PlayerId")]
        public virtual PlayerEntity Player { get; set; }
    }
}