﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GameServer.Models.Entities
{
    [Table("Player")]
    public class PlayerEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None), MaxLength(50)]
        public string PlayerId { get; set; }
        [Required, MaxLength(50)]
        public string Name { get; set; }
        [Required, MaxLength(2)]
        public string Gender { get; set; }
        [Required]
        public DateTime CreateTime { get; set; }
        [Required]
        [MaxLength(2)]
        public string Transportation { get; set; }


        public virtual ICollection<GameResultEntity> GameResults { get; set; }
    }
}