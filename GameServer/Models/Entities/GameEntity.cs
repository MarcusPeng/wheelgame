﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GameServer.Models.Entities
{
    [Table("Game")]
    public class GameEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None), MaxLength(50)]
        public string GameRoom { get; set; }
        [Required]
        public int BuyingSeconds { get; set; }
        [Required]
        public int RotateCountdownSeconds { get; set; }
        [Required]
        public int RotateSeconds { get; set; }
        [Required]
        public int ShowResultSeconds { get; set; }
        [Required, MaxLength(4000)]
        public string Wheel { get; set; }
        [Required, MaxLength(4000)]
        public string CardWinningSetting { get; set; }
        [Required]
        public DateTime CreateTime { get; set; }


        public virtual ICollection<GameResultEntity> GameResults { get; set; }
    }
}