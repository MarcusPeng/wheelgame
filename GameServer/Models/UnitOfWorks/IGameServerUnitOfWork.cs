﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameServer.Models.Entities;
using GameServer.Models.Repositories;


namespace GameServer.Models.UnitOfWorks
{
    public interface IGameServerUnitOfWork : IDisposable
    {
        IRepository<TEntity, TKey> GenerateRepository<TEntity, TKey>() where TEntity : class;
        int SaveChanges();
        Task<int> SaveChangesAsync();

        Task SavePlayerAsync(PlayerInfo playerInfo);
        Task SaveGameAsync(string GameRoom, WheelGameSetting wheelGameSetting);
        Task SaveGameResultAsync(string GameRoom, int GameSeq, int ResultColor, IEnumerable<PlayerInfo> PlayerInfoList);
        List<GameResultEntity> GetGameResult(string GameRoom);
    }
}
