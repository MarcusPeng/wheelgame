﻿using GameServer.Models;
using GameServer.Models.DbContexts;
using GameServer.Models.Entities;
using GameServer.Models.Repositories;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Unity;

namespace GameServer.Models.UnitOfWorks
{
    public class GameServerUnitOfWork : IGameServerUnitOfWork
    {
        private IGameServerContext _Context { get; set; }
        public GameServerUnitOfWork(IGameServerContext dbContext)
        {
            _Context = dbContext;
        }

        public int SaveChanges()
        {
            return _Context.SaveChanges();
        }
        public async Task<int> SaveChangesAsync()
        {
            return await _Context.SaveChangesAsync();
        }

        /// <summary>
        /// 產生Repository
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        public IRepository<TEntity, TKey> GenerateRepository<TEntity, TKey>() where TEntity : class
        {
            //Type RepositoryType = typeof(IRepository<TEntity, TKey>);
            //IRepository<TEntity, TKey> RepositoryInstance = System.Web.Mvc.DependencyResolver.Current.GetService(RepositoryType) as IRepository<TEntity, TKey>;

            return MvcApplication.Container.Resolve<IRepository<TEntity, TKey>>();
        }

        #region IDisposable Support
        private bool disposedValue = false; // 偵測多餘的呼叫

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: 處置受控狀態 (受控物件)。
                    _Context.Dispose();
                }

                // TODO: 釋放非受控資源 (非受控物件) 並覆寫下方的完成項。
                // TODO: 將大型欄位設為 null。

                disposedValue = true;
            }
        }

        // TODO: 僅當上方的 Dispose(bool disposing) 具有會釋放非受控資源的程式碼時，才覆寫完成項。
        // ~GameServerUnitOfWork() {
        //   // 請勿變更這個程式碼。請將清除程式碼放入上方的 Dispose(bool disposing) 中。
        //   Dispose(false);
        // }

        // 加入這個程式碼的目的在正確實作可處置的模式。
        public void Dispose()
        {
            // 請勿變更這個程式碼。請將清除程式碼放入上方的 Dispose(bool disposing) 中。
            Dispose(true);
            // TODO: 如果上方的完成項已被覆寫，即取消下行的註解狀態。
            GC.SuppressFinalize(this);
        }
        #endregion

        /// <summary>
        /// 保存玩家資料
        /// </summary>
        /// <param name="playerInfo"></param>
        public async Task SavePlayerAsync(PlayerInfo playerInfo)
        {
            await Task.Run(() =>
            {
                IRepository<PlayerEntity, string> PlayerRepository = GenerateRepository<PlayerEntity, string>();
                PlayerEntity Player = new PlayerEntity
                {
                    PlayerId = playerInfo.PlayerId,
                    Gender = ((int)playerInfo.Gender).ToString(),
                    Name = playerInfo.Name,
                    CreateTime = DateTime.Now,
                    Transportation = playerInfo.Transportation.ToString("d")
                };

                PlayerRepository.Insert(Player);
                PlayerRepository.SaveChangesAsync();
            });
        }

        /// <summary>
        /// 保存遊戲資料
        /// </summary>
        /// <param name="GameRoom"></param>
        /// <param name="wheelGameSetting"></param>
        public async Task SaveGameAsync(string GameRoom, WheelGameSetting wheelGameSetting)
        {
            await Task.Run(() =>
            {
                IRepository<GameEntity, string> GameRepository = GenerateRepository<GameEntity, string>();
                GameEntity Game = new GameEntity
                {
                    GameRoom = GameRoom,
                    BuyingSeconds = wheelGameSetting.BuyingSeconds,
                    RotateCountdownSeconds = wheelGameSetting.RotateCountdownSeconds,
                    RotateSeconds = wheelGameSetting.RotateSeconds,
                    ShowResultSeconds = wheelGameSetting.ShowResultSeconds,
                    CardWinningSetting = Newtonsoft.Json.JsonConvert.SerializeObject(wheelGameSetting.CardWinningSetting),
                    Wheel = Newtonsoft.Json.JsonConvert.SerializeObject(wheelGameSetting.Wheel),
                    CreateTime = DateTime.Now
                };
                GameRepository.Insert(Game);
                GameRepository.SaveChangesAsync();
            });
        }

        /// <summary>
        /// 保存遊戲結果
        /// </summary>
        /// <param name="GameRoom"></param>
        /// <param name="GameSeq"></param>
        /// <param name="ResultColor"></param>
        /// <param name="PlayerInfoList"></param>
        public async Task SaveGameResultAsync(string GameRoom, int GameSeq, int ResultColor, IEnumerable<PlayerInfo> PlayerInfoList)
        {
            IRepository<GameResultEntity, int> GameResultRepository = GenerateRepository<GameResultEntity, int>();
            foreach (PlayerInfo playerInfo in PlayerInfoList)
            {
                GameResultEntity GameResult = new GameResultEntity
                {
                    GameRoom = GameRoom,
                    GameSeq = GameSeq,
                    ResultColor = ResultColor,
                    PlayerId = playerInfo.PlayerId,
                    PlayerCoins = playerInfo.Coins,
                    PlayerOwnCards = Newtonsoft.Json.JsonConvert.SerializeObject(playerInfo.OwnCards),
                    CreateTime = DateTime.Now
                };
                GameResultRepository.Insert(GameResult);
            }
            await GameResultRepository.SaveChangesAsync();
        }

        public List<GameResultEntity> GetGameResult(string GameRoom)
        {
            IRepository<GameResultEntity, int> GameResultRepository = GenerateRepository<GameResultEntity, int>();
            return GameResultRepository.Query(x => x.GameRoom.Equals(GameRoom)).ToList();
        }
    }
}