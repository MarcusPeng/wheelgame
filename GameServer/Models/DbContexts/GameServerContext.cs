﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using GameServer.Models.Entities;

namespace GameServer.Models.DbContexts
{
    public class GameServerContext : DbContext, IGameServerContext
    {
        public GameServerContext() : base("GameServerContext")
        {

        }
        public DbSet<PlayerEntity> Player { get; set; }
        public DbSet<GameEntity> Game { get; set; }
        public DbSet<GameResultEntity> GameResult { get; set; }

    }
}