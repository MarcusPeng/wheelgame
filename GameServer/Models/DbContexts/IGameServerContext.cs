﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using GameServer.Models.Entities;

namespace GameServer.Models.DbContexts
{
    public interface IGameServerContext : IDisposable
    {
        DbSet<GameEntity> Game { get; set; }
        DbSet<GameResultEntity> GameResult { get; set; }
        DbSet<PlayerEntity> Player { get; set; }

        DbSet Set(Type entityType);
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}