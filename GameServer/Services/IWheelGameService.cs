﻿using GameServer.Games;
using GameServer.Models;
using GameServer.Models.Entities;
using GameServer.Transportation;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GameServer.Services
{
    public interface IWheelGameService
    {
        WebSocketTrans GenerateWebSocketHandler();
        void OnOpen(string Name, string PlayerId, PlayerInfo.GenderType gender, PlayerInfo.TransportationType transportationType, WebSocketTrans webSocketTrans = null);
        void OnClose(string PlayerId, PlayerInfo.TransportationType transportationType, WebSocketTrans webSocketTrans = null);
        void OnCardBuy(string PlayerId, WheelGame.WheelColor CardColor, PlayerInfo.TransportationType transportationType);
        void OnHistory(string PlayerId, string GameRoomm, PlayerInfo.TransportationType transportationType);
    }
}