﻿using GameServer.Games;
using GameServer.Models;
using GameServer.Models.Entities;
using GameServer.Models.UnitOfWorks;
using GameServer.Transportation;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Web.WebSockets;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace GameServer.Services
{
    public class WheelGameService : IWheelGameService
    {
        /// <summary>
        /// WebSocket連線
        /// </summary>
        private static readonly Dictionary<string, WebSocketCollection> _WebSocketDic = new Dictionary<string, WebSocketCollection>();
        /// <summary>
        /// 遊戲資訊
        /// </summary>
        private static readonly Dictionary<string, WheelGameInfo> _WheelGameInfoDic = new Dictionary<string, WheelGameInfo>();
        /// <summary>
        /// SignalR Hub
        /// </summary>
        private IHubContext _HubContext => GlobalHost.ConnectionManager.GetHubContext<WheelGameHub>();

        private readonly IGameServerUnitOfWork _GameServerUnitOfWork;

        public WheelGameService(IGameServerUnitOfWork gameServerUnitOfWork)
        {
            _GameServerUnitOfWork = gameServerUnitOfWork;
        }

        #region WebSocket Event
        /// <summary>
        /// 產生WebSocketHandler
        /// </summary>
        /// <param name="PlayerName"></param>
        /// <returns></returns>
        public WebSocketTrans GenerateWebSocketHandler()
        {
            WebSocketTrans webSocketTrans = new WebSocketTrans();
            webSocketTrans.OnOpenEvent += WebSocketTrans_OnOpenEvent;
            webSocketTrans.OnCloseEvent += WebSocketTrans_OnCloseEvent;
            webSocketTrans.OnCardBuyEvent += WebSocketTrans_OnCardBuyEvent;
            webSocketTrans.OnHistoryEvent += WebSocketTrans_OnHistoryEvent;

            return webSocketTrans;
        }

        private void WebSocketTrans_OnOpenEvent(object sender, OnOpenEventArgs e)
        {
            OnOpen(e.Name, e.PlayerId, e.Gender, PlayerInfo.TransportationType.WebSocket, (WebSocketTrans)sender);
        }
        private void WebSocketTrans_OnCloseEvent(object sender, EventArgs e)
        {
            WebSocketTrans webSocketTrans = (WebSocketTrans)sender;
            OnClose(webSocketTrans.PlayerId, PlayerInfo.TransportationType.WebSocket, webSocketTrans: webSocketTrans);
        }
        private void WebSocketTrans_OnCardBuyEvent(object sender, OnCardBuyEventArgs e)
        {
            WebSocketTrans webSocketTrans = (WebSocketTrans)sender;
            OnCardBuy(webSocketTrans.PlayerId, e.CardColor, PlayerInfo.TransportationType.WebSocket);
        }
        private void WebSocketTrans_OnHistoryEvent(object sender, OnHistoryEventArgs e)
        {
            WebSocketTrans webSocketTrans = (WebSocketTrans)sender;
            OnHistory(webSocketTrans.PlayerId, e.GameRoom, PlayerInfo.TransportationType.WebSocket);
        }
        #endregion

        #region Game Event
        private void WheelGame_OnStart(object sender, EventArgs e)
        {
            WheelGame wheelGame = (WheelGame)sender;
            string GameRoom = wheelGame.GameRoom;

            ClearPlayerOwnCard(GameRoom);

            Message message = new Message
            {
                EventType = nameof(WheelGame.WheelEventType.OnStart)
            };

            SendByGameRoom(message, GameRoom);
        }
        private void WheelGame_OnTickStart(object sender, EventArgs e)
        {
            WheelGame wheelGame = (WheelGame)sender;
            string GameRoom = wheelGame.GameRoom;

            Message message = new Message
            {
                EventType = nameof(WheelGame.WheelEventType.OnTickStart),
                Data = new
                {
                    wheelGame.LeftSeconds,
                    wheelGame.StepLeftSeconds,
                    _WheelGameInfoDic[GameRoom].Players
                }
            };

            SendByGameRoom(message, GameRoom);
        }
        private void WheelGame_OnBuyStart(object sender, EventArgs e)
        {
            WheelGame wheelGame = (WheelGame)sender;
            string GameRoom = wheelGame.GameRoom;

            Message message = new Message
            {
                EventType = nameof(WheelGame.WheelEventType.OnBuyStart)
            };

            SendByGameRoom(message, GameRoom);
        }
        private void WheelGame_OnBuyEnd(object sender, EventArgs e)
        {
            WheelGame wheelGame = (WheelGame)sender;
            string GameRoom = wheelGame.GameRoom;

            Message message = new Message
            {
                EventType = nameof(WheelGame.WheelEventType.OnBuyEnd)
            };

            SendByGameRoom(message, GameRoom);
        }
        private void WheelGame_OnRotateCountdownStart(object sender, EventArgs e)
        {
            WheelGame wheelGame = (WheelGame)sender;
            string GameRoom = wheelGame.GameRoom;

            Message message = new Message
            {
                EventType = nameof(WheelGame.WheelEventType.OnRotateCountdownStart),
                Data = new
                {
                    Duration = wheelGame.GameSetting.RotateSeconds,
                    wheelGame.ResultIndex
                }
            };

            SendByGameRoom(message, GameRoom);
        }
        private void WheelGame_OnRotateStart(object sender, EventArgs e)
        {
            WheelGame wheelGame = (WheelGame)sender;
            string GameRoom = wheelGame.GameRoom;

            Message message = new Message
            {
                EventType = nameof(WheelGame.WheelEventType.OnRotateStart)
            };

            SendByGameRoom(message, GameRoom);
        }
        private void WheelGame_OnShowResultStart(object sender, EventArgs e)
        {
            WheelGame wheelGame = (WheelGame)sender;
            string GameRoom = wheelGame.GameRoom;

            SummarizePlayerCoins(GameRoom);

            Message message = new Message
            {
                EventType = nameof(WheelGame.WheelEventType.OnShowResultStart)
            };

            SendByGameRoom(message, GameRoom);
        }

        #endregion


        /// <summary>
        /// 連線開啟處理
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="PlayerId"></param>
        /// <param name="gender"></param>
        /// <param name="transportationType"></param>
        /// <param name="webSocketTrans"></param>
        public void OnOpen(string Name, string PlayerId, PlayerInfo.GenderType gender, PlayerInfo.TransportationType transportationType, WebSocketTrans webSocketTrans = null)
        {
            PlayerInfo Player = new PlayerInfo
            {
                Name = Name,
                PlayerId = PlayerId,
                Gender = gender,
                Coins = 50,
                Transportation = transportationType
            };

            string GameRoom = AddToRoom(Player, webSocketTrans);
            WheelGame wheelGame = _WheelGameInfoDic[GameRoom].Game;
            Message message = new Message
            {
                EventType = "OnOpen",
                Data = new
                {
                    GameRoom,
                    wheelGame.GameSetting.Wheel,
                    wheelGame.GameSetting.CardWinningSetting,
                    wheelGame.CanBuy,
                    Player.PlayerId
                }
            };

            SendByPlayerId(message, PlayerId, Player.Transportation);

            _GameServerUnitOfWork.SavePlayerAsync(Player);
        }

        /// <summary>
        /// 連線結束處理
        /// </summary>
        /// <param name="PlayerId"></param>
        /// <param name="transportationType"></param>
        /// <param name="webSocketTrans"></param>
        public void OnClose(string PlayerId, PlayerInfo.TransportationType transportationType, WebSocketTrans webSocketTrans = null)
        {
            //remove from game
            string GameRoom = GetGameRoomById(PlayerId);
            PlayerInfo Player = _WheelGameInfoDic[GameRoom].Players.Single(x => x.PlayerId.Equals(PlayerId));
            _WheelGameInfoDic[GameRoom].Players.Remove(Player);

            //remove game
            if (_WheelGameInfoDic[GameRoom].Players.Count.Equals(0))
            {
                _WheelGameInfoDic[GameRoom].Game.Exit();
                _WheelGameInfoDic.Remove(GameRoom);
            }

            //remove transportation
            switch (transportationType)
            {
                case PlayerInfo.TransportationType.SignalR:
                    _HubContext.Groups.Remove(PlayerId, GameRoom);
                    break;

                case PlayerInfo.TransportationType.WebSocket:
                    _WebSocketDic[GameRoom].Remove(webSocketTrans);
                    if (_WebSocketDic[GameRoom].Count.Equals(0))
                    {
                        _WebSocketDic.Remove(GameRoom);
                    }
                    break;
            }
        }

        /// <summary>
        /// 買卡處理
        /// </summary>
        /// <param name="PlayerId"></param>
        /// <param name="CardColor"></param>
        /// <param name="transportationType"></param>
        public void OnCardBuy(string PlayerId, WheelGame.WheelColor CardColor, PlayerInfo.TransportationType transportationType)
        {
            string GameRoom = GetGameRoomById(PlayerId);
            if (_WheelGameInfoDic[GameRoom].Game.CanBuy)
            {
                PlayerInfo Player = _WheelGameInfoDic[GameRoom].Players.Single(x => x.PlayerId.Equals(PlayerId));
                if (Player.Coins > 0)
                {
                    Player.Coins -= 1;
                    switch (CardColor)
                    {
                        case WheelGame.WheelColor.Blue:
                            Player.OwnCards.Blue += 1;
                            break;
                        case WheelGame.WheelColor.Green:
                            Player.OwnCards.Green += 1;
                            break;
                        case WheelGame.WheelColor.Red:
                            Player.OwnCards.Red += 1;
                            break;
                        case WheelGame.WheelColor.Yellow:
                            Player.OwnCards.Yellow += 1;
                            break;
                        case WheelGame.WheelColor.Purple:
                            Player.OwnCards.Purple += 1;
                            break;
                        case WheelGame.WheelColor.Gold:
                            Player.OwnCards.Gold += 1;
                            break;
                        case WheelGame.WheelColor.Diamond:
                            Player.OwnCards.Diamond += 1;
                            break;
                    }
                }

                Message message = new Message
                {
                    EventType = "OnCardBuy",
                    Data = Player
                };

                SendByPlayerId(message, PlayerId, transportationType);
            }
        }

        public void OnHistory(string PlayerId, string GameRoomm, PlayerInfo.TransportationType transportationType)
        {
            Message message = new Message
            {
                EventType = "OnHistory",
                Data = GetGameHistory(GameRoomm)
            };

            SendByPlayerId(message, PlayerId, transportationType);
        }

        /// <summary>
        /// 取得歷史紀錄
        /// </summary>
        /// <param name="GameRoom"></param>
        /// <returns></returns>
        private List<GameResultHistory> GetGameHistory(string GameRoom)
        {
            List<GameResultEntity> GameResults = _GameServerUnitOfWork.GetGameResult(GameRoom);

            List<GameResultHistory> ResultHistoryList = new List<GameResultHistory>();
            GameResultHistory ResultHistory;
            GameResultEntity GameResult;

            foreach (int GameSeq in GameResults.Select(x => x.GameSeq).Distinct())
            {
                GameResult = GameResults.First(x => x.GameSeq.Equals(GameSeq));
                ResultHistory = new GameResultHistory
                {
                    GameRoom = GameResult.GameRoom,
                    GameSeq = GameResult.GameSeq,
                    ResultColor = GameResult.ResultColor,
                    Players = GameResults.Where(x => x.GameRoom.Equals(GameResult.GameRoom) && x.GameSeq.Equals(GameResult.GameSeq))
                     .Select(x => new PlayerInfo
                     {
                         Coins = x.PlayerCoins,
                         Gender = (PlayerInfo.GenderType)Convert.ToInt32(x.Player.Gender),
                         Name = x.Player.Name,
                         PlayerId = x.PlayerId,
                         OwnCards = JsonConvert.DeserializeObject<PlayerInfo.CardCount>(x.PlayerOwnCards)
                     }).ToList()
                };
                ResultHistoryList.Add(ResultHistory);
            }

            return ResultHistoryList;
        }

        /// <summary>
        /// 加入遊戲
        /// </summary>
        /// <param name="playerInfo"></param>
        /// <param name="webSocketTrans"></param>
        /// <returns></returns>
        private string AddToRoom(PlayerInfo playerInfo, WebSocketTrans webSocketTrans = null)
        {
            string GameRoom = string.Empty;
            KeyValuePair<string, WheelGameInfo> kvWheelGameInfo = _WheelGameInfoDic.SingleOrDefault(x => x.Value.Players.Count < x.Value.MaxPlayer);

            if (kvWheelGameInfo.Equals(default(KeyValuePair<string, WheelGameInfo>)))
            {
                WheelGameInfo wheelGameInfo = new WheelGameInfo
                {
                    Game = CreateWheelGame(),
                    Players = new List<PlayerInfo>() { playerInfo }
                };
                GameRoom = wheelGameInfo.Game.GameRoom;
                _WheelGameInfoDic.Add(GameRoom, wheelGameInfo);
            }
            else
            {
                GameRoom = kvWheelGameInfo.Key;
                kvWheelGameInfo.Value.Players.Add(playerInfo);
            }

            switch (playerInfo.Transportation)
            {
                case PlayerInfo.TransportationType.WebSocket:
                    if (_WebSocketDic.ContainsKey(GameRoom))
                    {
                        _WebSocketDic[GameRoom].Add(webSocketTrans);
                    }
                    else
                    {
                        _WebSocketDic.Add(GameRoom, new WebSocketCollection() { webSocketTrans });
                    }
                    break;

                case PlayerInfo.TransportationType.SignalR:
                    _HubContext.Groups.Add(playerInfo.PlayerId, GameRoom);
                    break;
            }

            return GameRoom;
        }

        /// <summary>
        /// 建立遊戲
        /// </summary>
        /// <returns></returns>
        private WheelGame CreateWheelGame()
        {
            WheelGame wheelGame = new WheelGame() { GameRoom = DateTime.Now.ToString("yyyyMMddHHmmssfff") };
            wheelGame.OnStart += WheelGame_OnStart;
            wheelGame.OnTickStart += WheelGame_OnTickStart;
            wheelGame.OnBuyStart += WheelGame_OnBuyStart;
            wheelGame.OnBuyEnd += WheelGame_OnBuyEnd;
            wheelGame.OnRotateCountdownStart += WheelGame_OnRotateCountdownStart;
            wheelGame.OnRotateStart += WheelGame_OnRotateStart;
            wheelGame.OnShowResultStart += WheelGame_OnShowResultStart;

            _GameServerUnitOfWork.SaveGameAsync(wheelGame.GameRoom, wheelGame.GameSetting);
            wheelGame.Start();

            return wheelGame;
        }

        /// <summary>
        /// 清空玩家持有卡片
        /// </summary>
        /// <param name="GameRoom"></param>
        private void ClearPlayerOwnCard(string GameRoom)
        {
            foreach (PlayerInfo Player in _WheelGameInfoDic[GameRoom].Players)
            {
                Player.OwnCards.Clear();
            }
        }

        /// <summary>
        /// 結算總金額
        /// </summary>
        /// <param name="GameRoom"></param>
        private void SummarizePlayerCoins(string GameRoom)
        {
            WheelGame wheelGame = _WheelGameInfoDic[GameRoom].Game;
            WheelGameSetting wheelGameSetting = wheelGame.GameSetting;
            WheelGame.WheelColor ResultColor = wheelGameSetting.Wheel[wheelGame.ResultIndex];
            IEnumerable<PlayerInfo> Players = _WheelGameInfoDic[GameRoom].Players;

            foreach (PlayerInfo Player in Players)
            {
                switch (ResultColor)
                {
                    case WheelGame.WheelColor.Blue:
                        Player.Coins += Player.OwnCards.Blue * wheelGameSetting.CardWinningSetting.Blue;
                        break;
                    case WheelGame.WheelColor.Green:
                        Player.Coins += Player.OwnCards.Green * wheelGameSetting.CardWinningSetting.Green;
                        break;
                    case WheelGame.WheelColor.Red:
                        Player.Coins += Player.OwnCards.Red * wheelGameSetting.CardWinningSetting.Red;
                        break;
                    case WheelGame.WheelColor.Yellow:
                        Player.Coins += Player.OwnCards.Yellow * wheelGameSetting.CardWinningSetting.Yellow;
                        break;
                    case WheelGame.WheelColor.Purple:
                        Player.Coins += Player.OwnCards.Purple * wheelGameSetting.CardWinningSetting.Purple;
                        break;
                    case WheelGame.WheelColor.Gold:
                        Player.Coins += Player.OwnCards.Gold * wheelGameSetting.CardWinningSetting.Gold;
                        break;
                    case WheelGame.WheelColor.Diamond:
                        Player.Coins += Player.OwnCards.Diamond * wheelGameSetting.CardWinningSetting.Diamond;
                        break;
                }
            }

            _GameServerUnitOfWork.SaveGameResultAsync(GameRoom, wheelGame.GameSeq, (int)ResultColor, Players);

            ClearPlayerOwnCard(GameRoom);
        }


        /// <summary>
        /// 發送訊息給單一玩家
        /// </summary>
        /// <param name="message"></param>
        /// <param name="PlayerId"></param>
        /// <param name="transportationType"></param>
        private void SendByPlayerId(Message message, string PlayerId, PlayerInfo.TransportationType transportationType)
        {
            switch (transportationType)
            {
                case PlayerInfo.TransportationType.WebSocket:
                    string GameRoom = GetGameRoomById(PlayerId);
                    WebSocketTrans webSocketTrans = _WebSocketDic[GameRoom].Single(x => x.WebSocketContext.SecWebSocketKey.Equals(PlayerId)) as WebSocketTrans;
                    webSocketTrans.Send(JsonConvert.SerializeObject(message));
                    break;

                case PlayerInfo.TransportationType.SignalR:
                    switch (message.EventType)
                    {
                        case "OnOpen":
                            _HubContext.Clients.Client(PlayerId).onOpen(message.Data);
                            break;

                        case "OnCardBuy":
                            _HubContext.Clients.Client(PlayerId).onCardBuy(message.Data);
                            break;
                    }
                    break;
            }
        }
        /// <summary>
        /// 發送訊息給遊戲房所有玩家
        /// </summary>
        /// <param name="message"></param>
        /// <param name="GameRoom"></param>
        private void SendByGameRoom(Message message, string GameRoom)
        {
            if (_WebSocketDic.ContainsKey(GameRoom))
            {
                _WebSocketDic[GameRoom].Broadcast(JsonConvert.SerializeObject(message));
            }

            switch (message.EventType)
            {
                case nameof(WheelGame.WheelEventType.OnStart):
                    _HubContext.Clients.Group(GameRoom).onStart(message.Data);
                    break;

                case nameof(WheelGame.WheelEventType.OnTickStart):
                    _HubContext.Clients.Group(GameRoom).onTickStart(message.Data);
                    break;

                case nameof(WheelGame.WheelEventType.OnBuyStart):
                    _HubContext.Clients.Group(GameRoom).onBuyStart(message.Data);
                    break;

                case nameof(WheelGame.WheelEventType.OnBuyEnd):
                    _HubContext.Clients.Group(GameRoom).onBuyEnd(message.Data);
                    break;

                case nameof(WheelGame.WheelEventType.OnRotateCountdownStart):
                    _HubContext.Clients.Group(GameRoom).onRotateCountdownStart(message.Data);
                    break;

                case nameof(WheelGame.WheelEventType.OnRotateStart):
                    _HubContext.Clients.Group(GameRoom).onRotateStart(message.Data);
                    break;

                case nameof(WheelGame.WheelEventType.OnShowResultStart):
                    _HubContext.Clients.Group(GameRoom).onShowResultStart(message.Data);
                    break;
            }
        }
        /// <summary>
        /// 取得玩家所在房號
        /// </summary>
        /// <param name="PlayerId"></param>
        /// <returns></returns>
        private string GetGameRoomById(string PlayerId)
        {
            return (from a in _WheelGameInfoDic
                    from b in a.Value.Players
                    where b.PlayerId == PlayerId
                    select a.Key).First();
        }


    }
}