﻿using Microsoft.Web.WebSockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GameServer.Services;
using GameServer.Models.Entities;
using System.Threading.Tasks;
using GameServer.Models;

namespace GameServer.Controllers
{
    public class GamesController : Controller
    {
        private readonly IWheelGameService _WheelGameService;
        public GamesController(IWheelGameService wheelGameService)
        {
            _WheelGameService = wheelGameService;
        }

        /// <summary>
        /// 遊戲列表
        /// </summary>
        /// <returns></returns>
        public ActionResult GameList()
        {
            return View();
        }

        /// <summary>
        /// 輪盤遊戲WebSocket版頁面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult WheelGameWebSocket()
        {
            return View();
        }

        /// <summary>
        /// 輪盤遊戲SignalR版頁面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult WheelGameSignalR()
        {
            return View();
        }

        /// <summary>
        /// 輪盤遊戲WebSocket連線
        /// </summary>
        /// <param name="PlayerName"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult WheelGameSocket(string PlayerName, string Gender)
        {
            var Result = new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);

            if (!string.IsNullOrEmpty(PlayerName) && int.TryParse(Gender, out int gender) && gender < 3)
            {
                if (System.Web.HttpContext.Current.IsWebSocketRequest || System.Web.HttpContext.Current.IsWebSocketRequestUpgrading)
                {
                    System.Web.HttpContext.Current.AcceptWebSocketRequest(
                         _WheelGameService.GenerateWebSocketHandler());

                    Result = new HttpStatusCodeResult(System.Net.HttpStatusCode.SwitchingProtocols);
                }
            }

            return Result;
        }

        /// <summary>
        /// Web Socket Test
        /// </summary>
        /// <returns></returns>
        public ActionResult WebSocketTest()
        {
            return View();
        }
    }
}