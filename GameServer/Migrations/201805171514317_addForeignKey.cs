namespace GameServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addForeignKey : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.GameResult", "GameRoome");
            CreateIndex("dbo.GameResult", "PlayerId");
            AddForeignKey("dbo.GameResult", "GameRoome", "dbo.Game", "GameRoom", cascadeDelete: true);
            AddForeignKey("dbo.GameResult", "PlayerId", "dbo.Player", "PlayerId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GameResult", "PlayerId", "dbo.Player");
            DropForeignKey("dbo.GameResult", "GameRoome", "dbo.Game");
            DropIndex("dbo.GameResult", new[] { "PlayerId" });
            DropIndex("dbo.GameResult", new[] { "GameRoome" });
        }
    }
}
