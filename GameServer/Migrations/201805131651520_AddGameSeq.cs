namespace GameServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGameSeq : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GameResult", "GameSeq", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.GameResult", "GameSeq");
        }
    }
}
