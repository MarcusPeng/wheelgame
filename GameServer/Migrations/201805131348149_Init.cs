namespace GameServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Game",
                c => new
                    {
                        GameRoom = c.String(nullable: false, maxLength: 50),
                        BuyingSeconds = c.Int(nullable: false),
                        RotateCountdownSeconds = c.Int(nullable: false),
                        RotateSeconds = c.Int(nullable: false),
                        ShowResultSeconds = c.Int(nullable: false),
                        Wheel = c.String(nullable: false, maxLength: 4000),
                        CardWinningSetting = c.String(nullable: false, maxLength: 4000),
                        CreateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.GameRoom);
            
            CreateTable(
                "dbo.GameResult",
                c => new
                    {
                        GameResultId = c.Int(nullable: false, identity: true),
                        GameRoome = c.String(nullable: false, maxLength: 50),
                        ResultColor = c.Int(nullable: false),
                        PlayerId = c.String(nullable: false, maxLength: 50),
                        PlayerCoins = c.Int(nullable: false),
                        PlayerOwnCards = c.String(nullable: false, maxLength: 4000),
                        CreateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.GameResultId);
            
            CreateTable(
                "dbo.Player",
                c => new
                    {
                        PlayerId = c.String(nullable: false, maxLength: 50),
                        Name = c.String(nullable: false, maxLength: 50),
                        Gender = c.String(nullable: false, maxLength: 2),
                        CreateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.PlayerId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Player");
            DropTable("dbo.GameResult");
            DropTable("dbo.Game");
        }
    }
}
