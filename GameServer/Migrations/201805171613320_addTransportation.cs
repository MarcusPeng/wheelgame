namespace GameServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTransportation : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.GameResult", name: "GameRoome", newName: "GameRoom");
            RenameIndex(table: "dbo.GameResult", name: "IX_GameRoome", newName: "IX_GameRoom");
            AddColumn("dbo.Player", "Transportation", c => c.String(nullable: false, maxLength: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Player", "Transportation");
            RenameIndex(table: "dbo.GameResult", name: "IX_GameRoom", newName: "IX_GameRoome");
            RenameColumn(table: "dbo.GameResult", name: "GameRoom", newName: "GameRoome");
        }
    }
}
