﻿using Owin;
using Microsoft.Owin;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Web.Mvc;

[assembly: OwinStartup(typeof(GameServer.Startup))]
namespace GameServer
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            GlobalHost.DependencyResolver.Register(typeof(IHubActivator), () => new MvcHubActivator());

            // Any connection or hub wire up and configuration should go here
            app.MapSignalR();
        }

        public class MvcHubActivator : IHubActivator
        {
            public IHub Create(HubDescriptor descriptor)
            {
                return (IHub)DependencyResolver.Current.GetService(descriptor.HubType);
            }
        }
    }
}