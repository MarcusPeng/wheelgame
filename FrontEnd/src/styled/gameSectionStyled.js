import styled , { css ,keyframes } from 'vue-styled-components';
import Vue from 'vue';

const sizes = {
  desktop: 1024,
  tablet: 769,
  phone: 375
};

const media = Object.keys(sizes).reduce((acc, label) => {
  	acc[label] = (...args) => css`
	    @media (max-width: ${sizes[label] / 16}em) {
	      ${css(...args)}
	    }
  	`
  	return acc
}, {});

const backgroundColor = '#2E3548';
const historyProps = { history: Boolean };

export const GameWrapper = styled('div', historyProps)`
	width:100%;
	max-width:1024px;
	height:1366px;
	margin:auto;
	background-color:${props=>props.history? 'rgba(0,0,0,.3)' : backgroundColor};
	font-family:Microsoft JhengHei;
	overflow:hidden;
	position:relative;

	${media.tablet`
		max-width:768px;
		height:100vh;

	`};

	${media.phone`
		max-width:375px;
		height:100vh;
	`};
`;

export const NameWrapper = styled.div`
	padding:20% 15%;
	box-sizing:border-box;
	width:100%;
	max-width:768px;
	height:100vh;
	margin:auto;
	background-color:${backgroundColor};
	font-family:Microsoft JhengHei;
	overflow:hidden;
	position:relative;

	label{
		width:49%;
		height:60px;
		line-height:60px;
		margin-top:50px;
		display:inline-block;
		font-size:36px;
		color:#dcdcdc;
		cursor:pointer;
		text-align:center;

		input{
			width:30px;
			height:30px;
			margin:10px;
		}
	}
	${media.tablet`
		padding:30% 25%;
		label{
			margin-top:50px;
			font-size:28px;

			input{
				width:20px;
				height:20px;
			}
		}
	`};
	${media.phone`
		padding:30% 20%;
		height:100vh;

		label{
			font-size:18px;
			margin-top:15px;
			float:left;
			height:40px;
			line-height:40px;
			input{
				width:15px;
				height:15px;
				margin:5px;
			}
		}
	`};
`;


export const PartWrapper = styled('div', historyProps)`
	width:80%;
	height:80%;
	background-color:${backgroundColor};
	border-radius:10px;
	position:relative;
	left:50%;
	top:50%;
	transform:translate(-50%,-50%);
	box-sizing:border-box;
	padding:${props=>props.history?'5%':'5% 10%'};
	font-size:20px;

	${media.phone`
		width:300px;
		height:500px;
		padding:5%;
		font-size:16px;
	`};
`;


export const InputStyle = styled.input`
	width:100%;
	height:70px;
	line-height:70px;
	border:none;
	background-color:transparent;
	border-bottom:1px solid #1d5d90;
	outline:none;
	color:#fff;
	font-size:34px;
	transition:.3s;
	box-sizing:border-box;
	cursor:pointer;
	margin-top:50px;


	${media.tablet`
		margin-top:30px;
	`};

	${media.phone`
		height:30px;
		line-height:30px;
		font-size:16px;
		margin-top:20px;
	`}
`;

export const TitleStyle = styled.div`
	color:#fff;
	width:100%;
	height:30px;
	line-height:30px;
	text-align:left;
	font-size:30px;

	${media.phone`
		font-size:20px;
	`};
`;

const titleProps = { right: Boolean };
export const SecondTitle = styled('div', titleProps)`
	color:#fff;
	width:50%;
	height:50px;
	line-height:50px;
	text-align:${props=>props.right?'right':'left'};
	font-size:20px;
	float:left;
	letter-spacing:1px;
	
	span{
		color:gold;
		margin:0 10px;
	}
	img{
		margin-bottom:-3px;
	}

	${media.phone`
		font-size:14px;
		height:30px;
		line-height:30px;
		letter-spacing:0;

		span{
			font-size:16px;
			margin:0 5px;
		}
	`};
`;


const nameStateProps = { nameState: Boolean };
export const BtnClickStyle = styled('div', nameStateProps)`
	width:60%;
	height:70px;
	line-height:70px;
	text-align:center;
	font-size:36px;
	color:#fff;
	background-color:${props=>props.nameState? '#1d5d90':'transparent'};
	border:1px solid #1d5d90;
	border-radius:5px;
	letter-spacing:2px;
	cursor:pointer;
	position:absolute;
	left:50%;
	top:40%;
	transform:translateX(-50%);
	
	${media.tablet`
		width:50%;
		top:50%;
		font-size:24px;
		height:50px;
		line-height:50px;
	`};

	${media.phone`
		width:60%;
		font-size:16px;
		top:46%;
		font-size:18px;
		height:35px;
		line-height:35px;
	`};
`;

export const LeftTimeWrapper = styled.div`
	width:350px;
	height:350px;
	position:absolute;
	top:10%;
	left:50%;
	transform:translateX(-50%);
	border-radius:50%;
	font-size:175px;
	line-height:350px;
	text-align:center;
	color:#fff;
	background-color:#1D5D90;

	span{
		position:absolute;
		line-height:450px;
		font-size:30px;
		color:#fff;
	}

	${media.phone`
		width:200px;
		height:200px;
		line-height:200px;
		font-size:70px;
			
			span{
				position:absolute;
				line-height:250px;
				font-size:20px;
			}
	`};
`; 

export const TipsStyle = styled.h3`
	color:#fff;
	width:100%;
	height:150px;
	text-align:center;
	position:absolute;
	line-height:1.5;
	bottom:7%;
	left:0;
	font-weight:300;
`;

const oddsProps = { odds: String };
export const CardBox = styled('div',oddsProps)`
	width:100%;
	height:50px;
	display:flex;
	justify-content:space-between;
	margin:3% 0;
	
	.color{
		width:70%;
		background-color:#1B141A;
		height:26px;
		margin:12px 0;
		border-radius:0 20px 20px 0;

		.winSetting{
			height:100%;
			line-height:50px;
			box-sizing:border-box;
			padding:0 5px;
			float:left;
			border-radius:0 20px 20px 0;

			span{
				color:gold;
				margin:0 5px;
			}
		}
		.odds{
			color:#fff;
			font-size:18px;
			float:left;
			margin-left:10px;
			letter-spacing:2px;
		}
	}
	.plus{
		width:40px;
		color:#fff;
		text-align:center;
		margin:5px 10px 5px 20px;
		border-radius:50%;
		background-color:#000;
		height:40px;
		line-height:35px;
		font-size:30px;
		cursor:pointer;
	}
	.haveCard{
		width:10%;
		color:#fff;
		height:50px;
		text-align:right;
		line-height:50px;
		color:gold;
		font-size:28px;
	}

	${media.phone`
		.plus{
			width:30px;
			height:30px;
			line-height:26px;
			margin:10px 10px 10px 20px;
		}
		.haveCard{
			line-height:1;
			padding:10px 0;
		}
	`};
`;

export const SettingWrapper = styled.div`
	position:absolute;
	top:0;
	right:0;
	height:50px;
	width:200px;
	border-radius:0 0 0 50px;
	box-sizing:border-box;
	padding:5px 10px;
	z-index:10;
	background-color:#2b2b2b;
	
	.icon{
		width:35px;
		height:35px;
		border-radius:50%;
		float:right;
		cursor:pointer;
		box-sizing:border-box;
		background-color:${backgroundColor};

		img{
			width:90%;
			height:90%;
			margin:5%;
		}
	}
	.home{
		margin-right:5%;

		img{
			width:80%;
			height:80%;
			margin:10%;
		}
	}

	${media.phone`
		width:100%;
		border-radius:0;
		padding:5px 10px;
		height:40px;
		
		.icon{
			width:30px;
			height:30px;
			margin-right:5px;
		}
		.home{
			float:left;
		}

	`};
`;


export const GameTimeWrapper = styled.div`
	width:300px;
	height:300px;
	position:absolute;
	top:100px;
	left:50%;
	transform:translateX(-50%);
	z-index:20;
	border-radius: 50%;

	.time{
		position:relative;
		left:50%;
		top:50%;
		transform:translate(-50%,-50%);
		border:10px solid #000;
		border-radius:50%;
		width:250px;
		height:250px;
		line-height:250px;
		color:#fff;
		font-size:60px;
		text-align:center;
		background:${backgroundColor};
	}

	${media.phone`
		width:150px;
		height:150px;
		top:85px;
		z-index:20;

		.time{
			width:130px;
			height:130px;
			line-height:130px;
			font-size:40px;
			text-align:center;
			border:6px solid #000;
		}

	`};
`;

export const ArrowStyle = styled.div`
	position:absolute;
	top:0;
	left:50%;
	transform:translateX(-50%);
	width: 0;
	height: 0;
	border-style: solid;
	border-width: 100px 50px 0 50px;
	border-color: #1d5d90 transparent transparent transparent;
	z-index:5;

	${media.phone`
		border-width: 80px 30px 0 30px;
		border-color: #1d5d90 transparent transparent transparent;
	`};
`;

const degProps = {'resultDeg':Number,'duration':Number};

export const CircleWrapper = styled('div',degProps)`
	width:300px;
	height:300px;
	background-color:#000;
	margin:auto;
	top:100px;
	position:relative;
	border-radius:50%;
	transform:rotate(87deg) scale(1.3);
	overflow:hidden;
	animation-name: wheelRotate;
	animation-duration: ${props=>props.duration-2}s;
	animation-iteration-count: 1;
	animation-timing-function: cubic-bezier(.22,.01,0,.99);
	animation-fill-mode: forwards;
	animation-delay: 5s;

	&:before{
		position:absolute;
		content:'';
		width:99%;
		height:99%;
		left:50%;
		top:50%;
		transform:translate(-50%,-50%);
		border:10px solid #000;
		box-shadow:0 0 10px 3px rgba(0,0,0,.5);
		overflow:hidden;
		border-radius:50%;
		z-index:5;
	}

	@keyframes wheelRotate {
	    from {transform:rotate(87deg) scale(1.3);}
   		to {transform:rotate(${props=>props.resultDeg}deg) scale(1.3);}
	}

	${media.phone`
		transform:rotate(87deg) scale(0.7);
		top:10px;

		@keyframes wheelRotate {
		    from {transform:rotate(87deg) scale(0.7);}
	   		to {transform:rotate(${props=>props.resultDeg}deg) scale(0.7);}
		}
	`};
`;

const divProps = { color: String };
export const ListSvg = styled('div', divProps)`
	width:150px;
	height:150px;
	position:absolute;
	transform-origin: 100% 100%;

	svg polygon{
		fill:${props=>props.color};
	}
`;


export const MemberWrapper = styled.div`
	width:100%;
	height:700px;
	margin-top:28%;
	display:inline-flex;
	justify-content:flex-start;
	align-content:flex-start;
	flex-wrap:wrap;
	position:relative;

	.box{
		width:45%;
		margin:1% 2%;
		height:100px;
		box-sizing:border-box;
		padding:10px;
		position:relative;
		border-radius:0 50px 50px 0;

		
		.nameBox{
			position:absolute;
			top:0;
			right:0;
			width:70%;
			height:50px;
			display:inline-flex;

			.name{
				width:50%;
				color:#fff;
				font-size:30px;
				height:40px;
				line-height:50px;
			}
			.coins{
				width:50%;
				color:#ff0;
				font-size:36px;
				height:50px;
				line-height:50px;
				text-align:right;
				border-radius:0 5px 0 0;
				box-sizing:border-box;
				padding:5px;
				margin-top:-3px;
				
				span{
					font-size:40px;
					margin-left:10px;
					float:right;
				}
				img{
					width:50px;
					height:50px;
					float:right;
				}

			}
		}
		.cardBox{
			width:85%;
			height:50px;
			position:absolute;
			bottom:0;
			right:0;
			border-radius:0 0 5px 0;
			border-right:1px solid #00B7CE;
			border-bottom:1px solid #00B7CE;
			background-color:#3f3f3f;
			box-sizing:border-box;
			padding:8px 0 0 15%;

			.card{
				width:35px;
				height:35px;
				border-radius:50%;
				text-align:center;
				line-height:35px;
				font-size:18px;
				color:#000;
				margin-left:1%;
				display:inline-block;
				box-shadow:0 0 5px 3px rgba(0,0,0,.2);
			}
		}	
	}

	.ownBox{
		width:96%;
		position:absolute;
		bottom:0;

		.nameBox{
			width:80%;
			.coins{
				width:60%;

				img{
					float:right;
					margin-right:20px;
				}
				span{
					margin-top:-12px;
					float:right;
					font-size:60px;
				}
			}
		}
		.cardBox{
			width:90%;
			padding:8px 0 0 45%;

			.card{
				margin-right:5%;
			}
		}
	}

	${media.tablet`
		height:500px;
		.box{
			.namebox{
				.name{
					width:50%;
				}
				.coins{
					font-size:24px;

					span{
						font-size:30px;
					}
					img{
						width:30px;
						height:30px;
					}
				}
			}
			.cardBox{
				width:100%;
				padding:8px 0 0 30%;

				.card{
					width:30px;
					height:30px;
					margin:-left:1%;
				}
			}
		}
		.ownBox{
			.nameBox{
				.name{
					width:50%;
				}
				.coins{
					
				}
			}
			.cardBox{
				width:100%;
				padding:8px 0 0 45%;
				.card{
					margin:0 0 0 5%;
					width:35px;
					height:35px;
				}
			}

		}
	`};

	${media.phone`
		height:350px;
		margin-top:0;
		top:78%;

		.box{
			height:60px;
			margin:2%;
			width:46%;
			border:1px solid #00B7CE;
			border-radius:5px;

			.nameBox{
				width:100%;
				height:30px;

				.name{
					width:50%;
					height:30px;
					font-size:14px;
					line-height:30px;
					padding:0 0 0 5px;
				}
				.coins{
					width:70%;
					height:30px;
					line-height:30px;
					font-size:25px;
					padding:0 10px 0 0;
					border:none;
					position:relative;

					img{
						width:25px;
						height:25px;
						margin:3px;
					}
					span{
						display:block;
						font-size:24px;
						margin:0;
						float:right;
					}	
				}
			}
			.cardBox{
				height:30px;
				border-radius:0 0 5px 5px;
				border:none;
				padding:0;
				width:100%;
				display:inline-flex;
				justify-content:space-around;

				.card{
					width:18px;
					height:18px;
					line-height:18px;
					font-size:14px;
					margin:5px 1.5% 0 0;
					font-weight:700;
				}
			}
		}
		.ownBox{
			width:96%;
			margin-top:0%;
			top:75%;

			.nameBox{

				.name{
					padding:0 0 0 25%;
				}
			}
			.cardBox{
				padding-left:25%;
				.card{

				}
			}
		}
	`};
`;

const ownProps = { own: Boolean ,Transportation : Number};
export const PhotoStyle = styled('div',ownProps)`
	position:absolute;
	width:${props=>props.own?'120px':'90px'};
	height:${props=>props.own?'120px':'90px'};
	background:${props=>props.Transportation == 0?'#54544B no-repeat 50% 50%/cover':'#ECECDE no-repeat 50% 50%/cover'};
	border-radius:50%;
	z-index:5;
	margin:${props=>props.own?'-4% 0 0 2%':'0'};


	${media.phone`
		display:${props=>props.own?'block':'none'};
		width:${props=>props.own?'70px':'50px'};
		height:${props=>props.own?'70px':'50px'};
		margin:-5% 0 0 0;
		left:0;

	`};
`;

export const ResultWrapper = styled('div',ownProps)`
	width:100%;
	height:70px;
	border:3px solid #2f2f2f;
	background-color:${props=>props.own?'#1D5D90':'transparent'};
	margin-top:${props=>props.own?'50px':'10px'};
	border-radius:50px;
	box-sizing:border-box;
	padding:5px;
	position:relative;
	overflow:hidden;

	.name{
		position:absolute;
		top:-1%;
		color:#fff;
		width:50%;
		height:70px;
		line-height:30px;
		box-sizing:border-box;
		padding-left:15px;

		span{
			position:absolute;
			height:60px;
			line-height:60px;
			font-size:30px;
		}
	}
	.coins{
		position:absolute;
		right:0;
		width:50%;
		top:0;
		height:70px;
		color:#fff;
		text-align:right;
		box-sizing:border-box;
		padding-right:10px;
		font-size:40px;
		font-weight:700;

		span{
			display:block;
			float:right;
			color:gold;
			margin:3px 20px 5px 10px;
			font-weight:700;
			font-size:40px;
		}
	}
	.cardbox{
		width:100%;
		height:30px;
		position:absolute;
		top:35px;
		display:inline-flex;
		justify-content:flex-start;

		.card{
			width:28px;
			height:28px;
			margin-right:5px;
			border-radius:50%;
			background-color:gold;
		}
	}

	${media.phone`
		height:50px;
		margin-top:${props=>props.own?'40px':'10px'};

		.name{
			width:60%;
			height:50px;
			line-height:50px;

			span{
				height:50px;
				line-height:40px;
				font-size:20px;
			}
		}
		.coins{
			height:45px;
			line-height:45px;
			font-size:28px;

			span{
				font-size:30px;
				margin:0 10px;
			}
		}
		.cardbox{
			height:20px;
			top:27px;
			line-height:20px;

			.card{
				width:20px;
				height:20px;
				line-height:20px;
			}
		}
	`}
`;

const winProps = { color: String };
export const ResultIndexStyle = styled('div',winProps)`
	width:170px;
	height:40px;
	position:absolute;
	top:5px;
	right:0;
	margin:5% 12%;
	border-radius:5px;
	background-color:#2b2b2b;
	
	.time{
		position:absolute;
		right:0;
		top:-35px;
		height:30px;
		width:330px;
		text-align:left;
		color:#dcdcdc;
		font-size:20px;
		float:right;
		white-space:nowrap;

	
		span{
			color:gold;
			margin:0 5px;
			font-size:24px;
		}
	}
	.text{
		width:70%;
		height:40px;
		float:left;
		font-size:18px;
		line-height:40px;
		float:right;
		color:#fff;
		padding-right:10px;
		box-sizing:border-box;
		text-align:right;
	}
	.ball{
		width:20px;
		height:20px;
		line-height:30px;
		margin:10px 15px 10px 10px;
		border-radius:50%;
		float:right;
		background-color:${props=>props.color};
	}
	.textRWD{
		display:none;
		float:left;
	}

	${media.phone`
		width:88px;
		margin:5%;
		height:30px;
		line-height:30px;
		
		.time{
			font-size:14px;
			top:29px;
			right:initial;
			left:-130px;

			span{
				font-size:16px;
			}
		}
		.text{
			font-size:14px;
			height:40px;
			line-height:40px;
			display:none;
		}
		.ball{
			width:20px;
			height:20px;
			margin:5px;
		}
		.textRWD{
			display:block;
			font-size:14px;
			height:30px;
			line-height:30px;
			color:#fff;
			width:60%;
		}
	`};

`;

export const HistoryWrapper = styled.div`
	width:100%;
	margin-top:20px;
	height:700px;
	overflow-y:auto;
	overflow-x:hidden;

	.boxWrapper{
		width:90%;
		border-bottom:1px solid #dcdcdc;
		padding:5% 5% 10% 5%;

		.seq{
			width:48%;
			height:30px;
			line-height:30px;
			font-size:18px;
			color:#dcdcdc;
			float:left;
			
		}
		.result{
			text-align:right;
			.text{
				float:right;
			}
			.color{
				width:20px;
				height:20px;
				border-radius:50%;
				float:right;
				margin:5px;
			}
		}
		.clear{
			clear:both;
		}
		.box{
			width:calc(100% - 15px);
			height:70px;
			border:1px solid rgba(255,255,255,.5);
			margin:5px 0 3%;
			box-shadow:0 0 5px 5px rgba(0,0,0,.2);
			
			.coins{
				width:20%;
				float:right;
				height:70px;
				line-height:70px;
				font-size:30px;
				text-align:right;
				margin-right:5%;
				color:gold;

				img{
					width:30px;
					height:30px;
					position:relative;
					top:3px;
				}
			}
			.cardbox{
				width:70%;
				height:70px;
				margin-left:5%;

				.name{
					font-size:20px;
					color:#dcdcdc;
					width:100%;
					float:left;
					margin:5px 0 3px;
					height:25px;
					line-height:25px;
				}

				.card{
					height:30px;
					line-height:30px;
					text-align:center;
					width:30px;
					display:inline-block;
					margin:0 2% 0 0;
					border-radius:50%;
					font-size:18px;
					color:#000;
				}
			}
		}
	}

	${media.phone`
		height:420px;
		margin-top:10px;
		.boxWrapper{
			.seq{
				font-size:14px;
			}
			.result{

				.text{
					
				}
				.color{
					width:20px;
					height:20px;
					border-radius:50%;
					float:right;
					margin:5px;
				}
			}
			.box{
				height:50px;

				.coins{
					height:50px;
					line-height:50px;
					font-size:20px;

					img{
						width:20px;
						height:20px;
						position:relative;
						top:3px;
					}
				}
				.cardbox{
					height:50px;

					.name{
						height:20px;
						line-height:20px;
						font-size:14px;
						margin:0;
					}
					.card{
						width:18px;
						height:18px;
						line-height:20px;
						font-size:14px;
						margin:0 1.5% 0 0;
					}
				}
			}
		}

	`};
`;

const popUpProps = {'index':Number };

export const GetResultWrapper = styled('div',popUpProps)`
	position:fixed;
	top:50%;
	left:50%;
	width:700px;
	height:300px;
	transform:translate(-50%,-50%);
	background-color:rgba(0,0,0,.7);
	z-index:${props=>props.index};
	
	.cong{
		width:100%;
		height:100px;
		line-height:100px;
		color:#fff;
		font-weight:500;
		letter-spacing:1px;
		font-size:28px;
		text-align:center;
	}
	.winText{
		height:20px;
		line-height:20px;
		font-size:24px;
	}
	.alert{
		height:auto;
		line-height:1.5;
		font-size:16px;
	}
	.color{
		width:50px;
		height:50px;
		border-radius:50%;
		margin:50px auto 0;
		text-align:center;
	}

	.clickBtn{
		display:block;
		text-decoration:none;
		width:200px;
		height:50px;
		text-align:center;
		line-height:50px;
		color:#dcdcdc;
		border-radius:5px;
		border:1px solid #dcdcdc;
		margin:30px auto;
		cursor:pointer;
	}

	${media.phone`
		width:300px;
		height:350px;
		
		.cong{
			font-size:20px;
		}

	`};	
`;